package delafoy.zabble;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZabbleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZabbleApplication.class, args);
	}
}
