module.exports = {
    entry: "./src/main/javascript/main.js",
    output: {
        path: __dirname+"/src/main/resources/static/dist/",
        publicPath: "dist/",
        filename: "bundle.js"
    },
    resolve: {
        extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js",".less"]
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: "style!css" },
            { test: /\.less$/, loader: "style!css!less" },
            { test: /\.tsx?$/, loader: "ts-loader" },
            { test: /\.jpg$/, loader: "file-loader" },
            { test: /\.png$/, loader: "url-loader?limit=100000" }
        ]
    }
};