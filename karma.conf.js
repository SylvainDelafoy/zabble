var webpackConfig = require('./webpack.config.js');

module.exports = function(config) {
	config.set({
		basePath : '',
		frameworks : [ 'jasmine' ],
		files : [ 'src/test/javascript/*.js' ],
		exclude : [ 'src/test/javascript/karma.conf*.js' ],
		reporters : [ 'progress' ],
		port : 9876,
		logLevel : config.LOG_INFO,
		browsers : [ 'PhantomJS' ],
		singleRun : false,
		autoWatch : true,
		plugins : [ 'karma-jasmine', 'karma-webpack',
				'karma-phantomjs-launcher' ],
		webpack : webpackConfig,
		preprocessors : {
			'src/test/javascript/**/*.js' : [ 'webpack' ],
			'src/test/javascript/**/*.jsx' : [ 'webpack' ]
		},
	});
};